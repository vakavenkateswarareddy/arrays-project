function map(elements, cb) {
    if (Array.isArray(elements) && elements.length!=0){
        let array=[];
        for (let index=0;index<elements.length;index++){
            let output=cb(elements[index],index,elements)
            array.push(output);
        }
        return array
    }else{
        return []
    }
}

module.exports=map;