function flattenFunction(array, depth =1) {
    if (!array || !Array.isArray(array)) {
      return [];
    }
    let isNested;
    let count = 0;
    do {
      let modifiedArray = [];
      isNested = false;
      for (let index = 0; index < array.length; index++) {
        if (Array.isArray(array[index])) {
          modifiedArray=modifiedArray.concat(array[index]);
          isNested = true;
        } else if (array[index] === undefined || array[index] === null) {
          continue;
        } else {
          modifiedArray.push(array[index]);
        }
      }
      count++;
      array = modifiedArray;
      if (count === depth || !isNested) {
        break;
      } else {
        continue;
      }
    } while (isNested);
    return array;
  }
  
  module.exports = flattenFunction;






















