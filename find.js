function find(elements, cb) {
    if (Array.isArray(elements) && elements.length!=0){
        for(let index=0;index<elements.length;index++){
           let output=cb(elements[index],index,elements);
           if(output){
               return elements[index];
           }
        }
    }else{
        return [];
    }
}

module.exports=find;