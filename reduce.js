function reduce(elements, cb, startingValue) {
    if (Array.isArray(elements) && elements.length!=0){
        if (startingValue===undefined){
            startingValue=elements[0]
            for(let i =1; i < elements.length; i++){
                startingValue=cb(startingValue,elements[i],i,elements);
            }
            return startingValue

        }else{
            for(let i = 0; i < elements.length; i++){
                startingValue=cb(startingValue,elements[i],i,elements)
            }
            return startingValue
        }
    }
    else{
        return [];
    }
}

module.exports=reduce;